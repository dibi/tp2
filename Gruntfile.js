module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			build: {
				src: 'js/*.js',
				dest: 'dist/<%= pkg.name %>.min.js'
			}
		},

		watch: {
			options: {
				livereload: true
			},
			scripts: {
				files: ['js/*.js'],
				tasks: ['uglify'],
			},
		},
		qunit_junit: {
			all: ['html/*.html']
		},
		qunit: {
			all: ['html/*.html']
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-qunit-junit');
	grunt.loadNpmTasks('grunt-contrib-qunit');

	grunt.registerTask('default', ['uglify']);

};

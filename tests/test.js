QUnit.test( "parity check test", function( assert )
{
	assert.expect( 15 );
	assert.ok(isEven(2));
	assert.ok(isEven(-4));
	assert.ok(isEven(50));
	assert.ok(isEven(728));
	assert.ok(isEven("84"));
	assert.ok(isEven('47789798'));
	assert.notOk(isEven("AAA"));
	assert.notOk(isEven("ZEEAEZAEZAEEZAE"));
	assert.notOk(isEven(1));
	assert.notOk(isEven(999999));
	assert.notOk(isEven(-888887));
	assert.notOk(isEven("00000.80000"));
	assert.notOk(isEven(0.8));
	assert.ok(isEven(8.0));
	assert.ok(isEven("6.0000000"));

});

QUnit.test( "bonjour test", function( assert )
{
	//assert.expect( 15 );
	assert.equal( hello("Greg"), "Bonjour, Greg" );
	assert.equal( hello(NaN), "Bonjour, NaN" );
	assert.equal( hello(), "Bonjour, undefined" );
	assert.equal( hello(0), "Bonjour, 0" );
	assert.equal( hello(1337), "Bonjour, 1337" );
});
